
This tiny git repositry includes the epfl printer ppd,
the server url and the commands to add them.

You must configure it to your needs by editing the `config.sh` file.

# Quick install
```sh
${EDITOR:-nano} config.sh
./install.sh
```

# Contributing
Just ask! Don't hesitate to make a merge request with your changes or
open a new issue.
