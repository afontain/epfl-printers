#!/usr/bin/env sh
setopt -e

. "$(dirname $0)"/config.sh

# you can configure this in config.sh or here
ppd_dir=${ppd_dir:-$(dirname $0)}

if ! which lpadmin >/dev/null; then
	echo "error: lpadmin is missing"
	exit 1
fi


$sudo lpadmin -p EPFL-BW    -E -v lpd://${gaspar}@${server}/SecurePrint-BW    -P "$ppd_dir"/PPD-6555i-bw.ppd
$sudo lpadmin -p EPFL-Color -E -v lpd://${gaspar}@${server}/SecurePrint-Color -P "$ppd_dir"/PPD-C5560i-color.ppd

# setting EPFL MyPrint as default printer
case "$default_printer" in
	(BW)
		$sudo lpadmin -d EPFL-BW ;;
	(Color)
		$sudo lpadmin -d EPFL-Color ;;
	("")
		;; # don't mess with default printer
	(*)
		echo "warning: \$default_printer must be either BW or Color;" \
		     "no default printer set" 1>&2
esac

echo "Added printers successfully"
